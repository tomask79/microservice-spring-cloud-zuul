# Writing MicroServices [part 8] #

## Spring Cloud Zuul as MicroService Gateway ##

When writing MicroServices you're going to be facing following problems:

* One request from client means to invoke multiple MicroServices
* You need mechanism of how to do [Canary Releasing](https://martinfowler.com/bliki/CanaryRelease.html)
* You need reverse proxy for invoking MicroServices

For all of the previous points can be used [Spring Cloud Zuul](http://cloud.spring.io/spring-cloud-netflix/spring-cloud-netflix.html)
Let's explore the last option.

## Spring Cloud Zuul as Reverse Proxy ##

When deploying MicroServices to Docker you're dealing with a problem that multiple MicroServices are mapped to your network to multiple ports. But your API consumers don't want to be bothered with that. They need to call everything at port 8080, for example. There are many solutions for that, but using of Spring Cloud Zuul really shines here!

Lets take previous demo of two MicroServices **citiesService** and **personsService** running at ports **8081** and **8082** and make reverse proxy for that in terms of that it's going to be possible to call both services under one port:

```
http://localhost:8080/cities  -> (redirect) -> http://localhost:8081/cities
http://localhost:8080/persons -> (redirect) -> http://localhost:8082/cities
```

Lets say this to Spring Cloud Zuul under the following configuration:

```
spring.application.name=personsService
eureka.client.serviceUrl.defaultZone=http://localhost:9761/eureka
eureka.client.healthcheck.enabled=true

zuul.routes.persons.path=/persons
zuul.routes.persons.serviceId=personsService

zuul.routes.cities.path=/cities
zuul.routes.cities.serviceId=citiesService

ribbon.eureka.enabled=true
server.port=8080
```

We're having here **two routes** with names **persons** and **cities**. Every call with path '/persons' will be redirected to personsService registered at Netflix Eureka server. And every call to '/cities' URL will be redirected to cititesService registered at Eureka. Pretty easy!

## Testing this demo ##

```
* Start RabbitMQ broker first (start depends on the environment you've got it installed in)
* git clone <this repo>
* mvn clean install (in the root folder with pom.xml)

* cd spring-microservice-registry
* java -jar target/registry-0.0.1-SNAPSHOT.war
verify that NetFlix Eureka is running at http://localhost:9761

* cd ..
* cd spring-microservice-config
* java -jar target/config-0.0.1-SNAPSHOT.war

* cd ..
* cd spring-microservice-service1
* java -jar target/service1-0.0.1-SNAPSHOT.war
verify at http://localhost:9761 that citiesService has been registered

* cd ..
* cd spring-microservice-service2
* java -jar target/service2-0.0.1-SNAPSHOT.war
verify at http://localhost:9761 that personsService has been registered

* cd ..
* cd spring-microservice-zuul
* java -jar target/spring-cloud-zuul-0.0.1-SNAPSHOT.war
(this will start your reverse proxy)
```

Now try to hit in the browser:

```
http://localhost:8080/persons
http://localhost:8080/cities
```

you should get then appropriate results...

```
{"persons":[{"name":"Tomas","surname":"Kloucek","department":"Programmer"}]}
resp
{"cities":[{"name":"Brno","state":"Czech republic"}]}
```

cheers

Tomas